const directory = './images/';

let list = new Array();

for (let i = 0; i < 10; i++) {
    list[i] = directory + i + '.png';
    new Image().src = list[i];
}

let counter = 0;

const start = document.getElementById('start');
start.addEventListener('click', () => {
    let random = setInterval(function() {
        counter++;
        let left = Math.floor(Math.random() * 10); 
        let center = Math.floor(Math.random() * 10); 
        let right = Math.floor(Math.random() * 10);

        document.left.src = list[left]; 
        document.center.src = list[center]; 
        document.right.src = list[right];

        if (counter > 5){
            let finalLeft = list[left]; 
            let finalCenter = list[center]; 
            let finalRight = list[right];
            if ((finalLeft == finalCenter) || (finalLeft == finalRight) || (finalCenter == finalRight) ) { 
                document.getElementById("all").innerHTML = "You won second prize!"; 
                document.getElementById("all").style.color = "green"; 

                } else if ((finalLeft == finalCenter) && (finalLeft == finalRight) ) {  
                    document.getElementById("all").innerHTML = "You won first prize!";
                    document.getElementById("all").style.color = "green"; 

                 } else  { 
                     document.getElementById("all").innerHTML = "You lost. Try Again!"; 
                    document.getElementById("all").style.color = "red"; 
                 } 
              
                 counter = 0; 
                 clearInterval(random); 

          }
   }, 100);  
})

const loginSubmit = document.getElementById('loginSubmit');
const loginDiv = document.getElementById('loginDiv');

loginSubmit.addEventListener('click', () => {
    loginDiv.innerHTML = '<h3><a href="#">Log out</a> when finished!</h3><br><span>Login Successful!</span>';
})
